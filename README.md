# SimpleModal OSX Style Dialog

### OSX Style Modal Dialog

A modal dialog configured to behave like an OSX dialog. Demonstrates the use of the callbacks as well as custom styling and a handful of options
Inspired by [ModalBox](http://okonet.ru/projects/modalbox/) and OSX style dialog built with [Prototype](http://www.prototypejs.org/)

* SimpleModal is a lightweight jQuery Plugin which provides a powerful interface for modal dialog development. Think of it as a modal dialog framework.

* SimpleModal gives you the flexibility to build whatever you can envision, while shielding you from related cross-browser issues inherent with UI development.

* As you can see by this example, SimpleModal can be easily configured to behave like an OSX dialog. With a handful options, 2 custom callbacks and some styling, you have a visually appealing dialog that is ready to use!

[DEMO](https://readloud.github.io/SimpleModal-OSX-Style-Dialog/)
